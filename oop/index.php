<?php

require_once('frog.php');
require_once('ape.php');

$animal = new Animal("shaun");

echo "Name : ". $animal->name;
echo "<br>Legs : ". $animal->legs;
echo "<br>Cold Blooded : ". $animal->cold_blooded;

echo "<br><br>";

$frog = new Frog("buduk");

echo "Name : ". $frog->name;
echo "<br>Legs : ". $frog->legs;
echo "<br>Cold Blooded : ". $frog->cold_blooded;
echo "<br>Jump : ". $frog->jump();

echo "<br><br>";

$ape = new Ape("kera sakti");

echo "Name : ". $ape->name;
echo "<br>Legs : ". $ape->legs;
echo "<br>Cold Blooded : ". $ape->cold_blooded;
echo "<br>Yell : ". $ape->yell();
