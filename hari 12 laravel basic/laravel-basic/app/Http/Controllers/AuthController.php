<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function store(Request $request){

        // dd($request->all());
        $bahasa = json_encode($request->language);
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        // foreach ($request['language'] as $key => $value) {
        //     array_push($bahasa,['language' => $value]);
        // }
        $bio = $request['bio'];
        return view('halaman.welcome', [
            'firstName' => $firstName,
            'lastName' => $lastName,
            'gender' => $gender,
            'bahasa' => $bahasa,
            'nationality' => $nationality,
            'bio' => $bio,

        ]);
    }

    public function welcome(){
        return view('halaman.welcome');
    }
}
