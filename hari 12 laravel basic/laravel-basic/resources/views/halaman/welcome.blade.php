@extends('layout.master')
@section('judul')
Halaman Utama
@endsection

@section('content')
<h1>SELAMAT DATANG {{ $firstName }} {{ $lastName }}</h1>
<h2>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h2>
<table width="600px">
    <tr>
        <td>Nama Depan</td>
        <td>{{ $firstName }}</td>
    </tr>
    <tr>
        <td>Nama Belakang</td>
        <td>{{ $lastName }}</td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        @if ($gender == 0)
        <td>Laki-laki</td>
        @else
        <td>Perempuan</td>
        @endif
    </tr>
    <tr>
        <td>Kewarganegaraan</td>
        <td>{{ $nationality }}</td>
    </tr>
    <tr>
        <td>Bahasa</td>
        <td>@php
            $languages = json_decode($bahasa)
            @endphp
            @foreach ($languages as $language)
            <li>{{ $language }} </li>
            @endforeach
        </td>
    </tr>
    <tr>
        <td>Bio</td>
        <td>{{ $bio }}</td>
    </tr>
</table>
@endsection