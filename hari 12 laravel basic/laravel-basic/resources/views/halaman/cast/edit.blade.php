@extends('layout.master')
@section('judul')
Halaman Edit Cast
@endsection
@section('content')
<div>
    <h2>Ubah Data</h2>
    <form action="{{ route('cast.update', $cast->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" value="{{ $cast->nama }}" name="nama" id="nama" placeholder="Masukkan Nama">
            @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" value="{{ $cast->umur }}" name="umur" id="umur" placeholder="Masukkan Umur">
            @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" value="{{ $cast->bio }}" name="bio" id="bio" placeholder="Masukkan Bio">
            @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
</div>
@endsection