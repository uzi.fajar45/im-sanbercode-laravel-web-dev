@extends('layout.master')
@section('judul')
Halaman Cast
@endsection
@push('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
@endpush

@push('js')
<script>
    $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    const swal = $('.swal').data('swal');
        if (swal) {
            Swal.fire({
                'title': 'success',
                'text': swal,
                'icon': 'success'
            })
        }
</script>
@endpush
@section('content')
<div class="swal" data-swal="{{ session('success') }}"></div>
<a href="{{ route('cast.create') }}" class="btn btn-primary mb-2">Tambah</a>
<table class="table" id="myTable">
    <thead class="thead-light">
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key)
        <tr>
            <td>{{$loop->iteration}}</th>
            <td>{{$key->nama}}</td>
            <td>{{$key->umur}}</td>
            <td>{{$key->bio}}</td>
            <td>
                <form action="/cast/{{$key->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/cast/{{$key->id}}" class="btn btn-info">Show</a>
                    <a href="/cast/{{$key->id}}/edit" class="btn btn-primary">Edit</a>
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <tr colspan="3">
            <td>No data</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection