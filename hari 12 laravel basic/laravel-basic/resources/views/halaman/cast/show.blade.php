@extends('layout.master')
@section('judul')
Halaman Show Cast
@endsection
@section('content')
<table class="table table-stripped table-bordered bg-white">
    <tr>
        <th width="250px">Nama</th>
        <td>{{ $cast->nama }}</td>
    </tr>
    <tr>
        <th>Umur</th>
        <td>{{ $cast->umur }}</td>
    </tr>
    <tr>
        <th>Bio</th>
        <td>{{ $cast->bio }}</td>
    </tr>
</table>
@endsection