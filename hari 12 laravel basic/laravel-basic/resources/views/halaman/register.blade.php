@extends('layout.master')
@section('judul')
Halaman Utama
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="{{ url('welcome') }}" method="POST">
        @csrf
        <label for="firstName">First Name:</label> <br><br>
        <input type="text" name="firstName" placeholder="First Name..." required><br>
        <br>
        <label for="lastName">Last Name:</label> <br><br>
        <input type="text" name="lastName" placeholder="Last Name..." required><br>
        <br>
        <Label for="gender">Gender:</Label><br><br>
        <input type="radio" name="gender" value="0" required>Male<br>
        <input type="radio" name="gender" value="1">Female<br>
        <br>
        <Label for="nationality">Nationality:</Label><br><br>
        <select name="nationality" id="nationality">
            <option value="none" hidden>-- Pilih --</option>
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>
        <br><br>
        <Label for="language">Language Spoken:</Label><br><br>
        <input type="checkbox" name="language[]" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="language[]" value="English">English<br>
        <input type="checkbox" name="language[]" value="Japanese">Japanese<br>
        <br>
        <Label for="bio">Bio:</Label><br><br>
        <textarea name="bio" id="" cols="30" rows="10" required></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
    @endsection