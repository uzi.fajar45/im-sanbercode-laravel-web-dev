<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [HomeController::class,'index']);
Route::get('/register', [AuthController::class,'register']);
Route::post('/welcome', [AuthController::class,'store']);

Route::get('/', function (){
    return view('halaman.home');
});

Route::get('/table', function (){
    return view('halaman.table');
});

Route::get('/data-table', function (){
    return view('halaman.datatable');
});

Route::resource('/cast', CastController::class);